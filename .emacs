;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Emacs 24
;;
;;  By Gravemind2a <gravemind2a@gmail.com>
;;  https://github.com/Gravemind/ArchLinux
;;
;;  Sources and inspirations:
;;  23.2:
;;      http://alexott.net/en/writings/emacs-devenv/EmacsCedet.html
;;      http://www.xemacs.org/Documentation/packages/html/semantic_11.html
;;      auto-complete-clang:
;;          http://mike.struct.cn/blogs/entry/15/
;;          https://github.com/mikeandmore/auto-complete-clang
;;  24:
;;      http://emacs-fu.blogspot.com/
;;
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(modify-frame-parameters nil '((wait-for-wm . nil)))

(add-to-list 'load-path "~/.emacs.d/config")
(require 'jo-config-ide)
